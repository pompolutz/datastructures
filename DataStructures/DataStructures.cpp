#include "stdafx.h"
#include "memory"
#include <iostream>
#include <cassert>
#include "SinglyLinkedList.h"

bool given_empty_singly_linked_list_should_pop_front_nullptr()
{
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    auto head = sll->TopFront();

    return !head;
}

bool given_empty_singly_linked_list_should_pop_back_nulltpr()
{
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    auto tail = sll->TopBack();

    return !tail;
}

bool given_sll_with_one_element_pushed_front_should_return_that_element_as_head()
{
    auto number = 42;
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    sll->PushFront(number);

    auto head = sll->TopFront();

    return head && head->data == number;
}

bool given_sll_with_one_element_pushed_front_should_return_that_element_as_tail()
{
    auto number = 42;
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    sll->PushFront(number); 

    auto tail = sll->TopBack();
    return tail && tail->data == number;
}

bool given_sll_with_pushed_front_elements_should_return_head_same_as_last_pushed()
{
    auto first = 1;
    auto second = 2;
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    sll->PushFront(first);
    sll->PushFront(second);

    auto head = sll->TopFront();
    return head && head->data == second;
}

bool given_sll_with_pushed_front_elements_should_return_tail_same_as_the_first_pushed()
{
    auto first = 1;
    auto second = 2;
    auto sll = std::make_unique<SinglyLinkedList<int>>();
    sll->PushFront(first);
    sll->PushFront(second);

    auto tail = sll->TopBack();
    return tail && tail->data == first;
}

int main()
{
    std::cout << "Singly-linked list. Initialization tests" << std::endl;
    assert(given_empty_singly_linked_list_should_pop_front_nullptr());
    assert(given_empty_singly_linked_list_should_pop_back_nulltpr());

    std::cout << "Singly-linked list. Pushing at front tests" << std::endl;
    assert(given_sll_with_one_element_pushed_front_should_return_that_element_as_head());
    assert(given_sll_with_one_element_pushed_front_should_return_that_element_as_tail());
    assert(given_sll_with_pushed_front_elements_should_return_head_same_as_last_pushed());
    assert(given_sll_with_pushed_front_elements_should_return_tail_same_as_the_first_pushed());

    return 0;
}

