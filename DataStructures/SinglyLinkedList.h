#pragma once
#include <memory>

template <typename T>
class SinglyLinkedList
{
    struct Node
    {
        Node() : data(0) {}
        Node(T value) : data(value) {}
        Node(const Node& other) : data(other.data) {}

        T data;
        std::shared_ptr<Node> next;
    };

public:
    void PushFront(T value);
    void PushBack(T value);
    std::shared_ptr<Node> TopFront() const;
    void PopFront();
    std::shared_ptr<Node> TopBack() const;
    void PopBack();
    bool Find(T value);
    void Erase(T value);
    bool CheckIsEmpty();
    void AddBefore(const Node& node, T value);

private:
    std::shared_ptr<Node> head;
};

template <typename T>
void SinglyLinkedList<T>::PushFront(T value)
{
    if (!head)
    {
        head = std::make_shared<Node>();
        auto newHead = std::make_shared<Node>(value);
        head->next = newHead;
    }
    else
    {
        auto newHead = std::make_shared<Node>(value);
        newHead->next = head->next;
        head->next = newHead;
    }
}

template <typename T>
void SinglyLinkedList<T>::PushBack(T value)
{
    if (!head)
    {
        head = std::make_shared<Node>();
        auto newHead = std::make_shared<Node>(value);
        head->next = newHead;
    }
    else
    {
        auto tail = head->next;
        while (tail->next)
        {
            tail = tail->next;
        }

        tail->next = std::make_shared<Node>(value);
    }
}

template <typename T>
std::shared_ptr<typename SinglyLinkedList<T>::Node> SinglyLinkedList<T>::TopFront() const
{
    return head ? head->next : nullptr;
}

template <typename T>
void SinglyLinkedList<T>::PopFront()
{
    if (head)
        head = head->next;
}

template <typename T>
std::shared_ptr<typename SinglyLinkedList<T>::Node> SinglyLinkedList<T>::TopBack() const
{
    auto head = TopFront();
    if (!head)
        return nullptr;

    if (!head->next)
        return head;

    auto tail = head->next;
    while (tail->next)
    {
        tail = tail->next;
    }

    return tail;
}
